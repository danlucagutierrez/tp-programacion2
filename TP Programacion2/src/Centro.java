
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Centro {

	private String cuit;
	private String nombre;
	private int valorDiaInternacion;
	private LinkedList<Especialidad> especialidades;
	private HashMap<Integer, Paciente> pacientes;
	private HashMap<Integer, Medico> medicos;
	private List<Integer> internadosHastaHoy;

	public Centro(String nombre, String cuit, int valorDiaInternacion) {

		this.nombre = nombre;
		this.cuit = cuit;
		this.valorDiaInternacion = valorDiaInternacion;
		this.especialidades = new LinkedList<Especialidad>();
		this.pacientes = new HashMap<Integer, Paciente>();
		this.medicos = new HashMap<Integer, Medico>();
		this.internadosHastaHoy = new ArrayList<Integer>();
	}

	/* Sobreescritura */
	/* StringBuilder */
	@Override
	public String toString() {

		StringBuilder s = new StringBuilder("Clinica: ");
		return s.append(this.nombre).toString();
	}

	public void agregarEspecialidad(String nombre, double valorConsulta) {

		this.especialidades.addLast(new Especialidad(nombre, valorConsulta));
	}

	public void agregarPacienteObraSocial(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento,
			String obraSocial, double porcentaje) {

		this.pacientes.put(historiaClinicaPaciente,
				new PacienteObraSocial(nombre, historiaClinicaPaciente, fechaNacimiento, obraSocial, porcentaje));
	}

	public void agregarPacienteAmbulatorio(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento) {

		this.pacientes.put(historiaClinicaPaciente,
				new PacienteAmbulatorio(nombre, historiaClinicaPaciente, fechaNacimiento));
	}

	public void agregarPacientePrivado(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento) {

		this.pacientes.put(historiaClinicaPaciente,
				new PacientePrivado(nombre, historiaClinicaPaciente, fechaNacimiento));
	}

	/* Iterator */
	public void agregarMedico(String nombre, int matricula, String nombreEspecialidad, double valorHonorario) {

		Iterator<Especialidad> it = this.especialidades.iterator();

		while (it.hasNext()) {

			Especialidad posibleEspecialidad = it.next();

			if (nombreEspecialidad.equals(posibleEspecialidad.retornarNombre())) {

				this.medicos.put(matricula, new Medico(nombre, matricula, nombreEspecialidad, valorHonorario));
			}
		}
	}

	public Paciente buscarPaciente(int historiaClinicaPaciente) {

		return pacientes.get(historiaClinicaPaciente);
	}

	public Medico buscarMedico(int matricula) {

		return medicos.get(matricula);
	}

	/* Utilizar firma correspondiente a la interfaz de cliente */
	public double getSaldo(int historiaClinicaPaciente) {

		return estadoCuentaPaciente(historiaClinicaPaciente);
	}

	public double estadoCuentaPaciente(int historiaClinicaPaciente) {

		return buscarPaciente(historiaClinicaPaciente).retornarSaldo();
	}

	/* Utilizar firma correspondiente a la interfaz de cliente */
	public void pagarSaldo(int historiaClinicaPaciente) {

		pagarCuentaPaciente(historiaClinicaPaciente);
	}

	public void pagarCuentaPaciente(int historiaClinicaPaciente) {

		Paciente paciente = buscarPaciente(historiaClinicaPaciente);

		paciente.ponerEnCero();
	}

	public void agregarTratamiento(int historiaClinicaPaciente, int matricula, String nombreTratamiento) {

		Paciente posiblePacienteAmbulatorio = buscarPaciente(historiaClinicaPaciente);

		Medico posibleMedico = buscarMedico(matricula);

		if (posiblePacienteAmbulatorio instanceof PacienteAmbulatorio && posibleMedico instanceof Medico) {

			((PacienteAmbulatorio) posiblePacienteAmbulatorio).agregarTratamiento(matricula, nombreTratamiento);

			((PacienteAmbulatorio) posiblePacienteAmbulatorio).modificarSaldo(posibleMedico.retornarValorHonorario());
		}
	}

	public void agregarInternacion(int historiaClinicaPaciente, String area, Fecha fechaIngreso) {

		Paciente posiblePacienteObraSocial = buscarPaciente(historiaClinicaPaciente);

		if (posiblePacienteObraSocial instanceof PacienteObraSocial) {

			PacienteObraSocial POS = (PacienteObraSocial) posiblePacienteObraSocial;

			LinkedList<RegistroInternacion> listaInternaciones = ((PacienteObraSocial) posiblePacienteObraSocial)
					.retornarRegistroInternacion();

			if (listaInternaciones.isEmpty()) {

				POS.agregarIngresoInternacion(area, fechaIngreso);

			} else {

				if (listaInternaciones.getLast().retornarFechaAlta() == null) {

					throw new RuntimeException("El paciente permanece internado.");

				} else {

					if (fechaIngreso.esMenor(listaInternaciones.getLast().retornarFechaAlta())) {

						throw new RuntimeException("El paciente permanece internado.");

					} else {

						if (listaInternaciones.getLast().retornarFechaAlta().esMenor(fechaIngreso)) {

							POS.agregarIngresoInternacion(area, fechaIngreso);
						}
					}
				}
			}
		}
	}

	public void altaInternacion(int historiaClinicaPaciente, Fecha fechaAlta) {

		Paciente posiblePacienteObraSocial = buscarPaciente(historiaClinicaPaciente);

		if (posiblePacienteObraSocial instanceof PacienteObraSocial) {

			PacienteObraSocial POS = (PacienteObraSocial) posiblePacienteObraSocial;

			LinkedList<RegistroInternacion> listaInternaciones = POS.retornarRegistroInternacion();

			if (listaInternaciones.getLast().retornarFechaAlta() == null) {

				listaInternaciones.getLast().modificarAltaInternacion(fechaAlta);

				POS.modificarSaldo(
						POS.retornarPorcentaje() * Fecha.diasEntre(listaInternaciones.getLast().retornarFechaIngreso(),
								listaInternaciones.getLast().retornarFechaAlta()) + this.valorDiaInternacion);
			}
		}
	}

	Map<Fecha, String> atencionesEnConsultorio(int historiaClinicaPaciente) {

		HashMap<Fecha, String> atenciones = new HashMap<Fecha, String>();

		Paciente posiblePacientePrivado = buscarPaciente(historiaClinicaPaciente);

		if (posiblePacientePrivado instanceof PacientePrivado) {

			PacientePrivado PP = (PacientePrivado) posiblePacientePrivado;

			for (RegistroEspecialidad registro : PP.retornarRegistroEspecialidades()) {

				atenciones.put(registro.retornarFechaAtencion(),
						buscarMedico(registro.retornarMatricula()).retornarNombre());
			}
		}
		return atenciones;
	}

	// Agrega atencion de especialidad
	public void agregarAtencion(int historiaClinicaPaciente, Fecha fecha, int matricula) {

		Paciente posiblePacientePrivado = buscarPaciente(historiaClinicaPaciente);

		if (posiblePacientePrivado instanceof PacientePrivado) {

			PacientePrivado PP = (PacientePrivado) posiblePacientePrivado;

			for (RegistroEspecialidad registro : PP.retornarRegistroEspecialidades()) {

				if (fecha.equals(registro.retornarFechaAtencion())) {

					throw new RuntimeException("Puede tener solo una atenci�n por fecha");
				}
			}
			Medico posibleMedico = buscarMedico(matricula);

			if (posiblePacientePrivado instanceof PacientePrivado && posibleMedico instanceof Medico) {

				String especialidad = posibleMedico.retornarEspecialidad();

				for (Especialidad nombreEspecialidad : this.especialidades) {

					if (especialidad.equals(nombreEspecialidad.retornarNombre())) {

						((PacientePrivado) posiblePacientePrivado).guardarRegistroEspecialidad(fecha, matricula);

						((PacientePrivado) posiblePacientePrivado)
								.modificarSaldo(nombreEspecialidad.retornarValorConsulta());
					}
				}
			}
		}
	}

	// Sobrecarga de m�todos
	// Agrega atencion de guardia
	public void agregarAtencion(int historiaClinicaPaciente, Fecha fecha) {

		Paciente posiblePacientePrivado = buscarPaciente(historiaClinicaPaciente);

		if (posiblePacientePrivado instanceof PacientePrivado) {

			((PacientePrivado) posiblePacientePrivado).guardarRegistrosGuardia(fecha);
		}
	}

	public List<Integer> listaInternacion() {

		for (Integer historiaClinica : pacientes.keySet()) {

			Paciente posiblePacienteObraSocial = buscarPaciente(historiaClinica);

			if (posiblePacienteObraSocial instanceof PacienteObraSocial) {

				PacienteObraSocial POS = (PacienteObraSocial) posiblePacienteObraSocial;

				if (POS.estaInternado()) {

					this.internadosHastaHoy.add(POS.historiaClinicaPaciente);
				}
			}
		}
		return this.internadosHastaHoy;
	}
}