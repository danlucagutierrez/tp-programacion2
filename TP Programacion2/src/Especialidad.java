
public class Especialidad {

	/*Se saco el atributo codigo para esta segunda parte por que cambiaron los requerimientos*/
	private String nombre;
	private double valorConsulta;

	public Especialidad(String nombre, double valorConsulta) {

		this.nombre = nombre;
		this.valorConsulta = valorConsulta;
	}

	public String retornarNombre() {
		
		return this.nombre;
	}

	public double retornarValorConsulta() {
		
		return this.valorConsulta;
	}
	
	@Override
	public String toString() {
		
		return this.nombre;
	}
}