
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

public class Fecha {

	private int dia;
	private int mes;
	private int anio;

	public Fecha(int dia, int mes, int anio) {

		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		LocalDate.of(this.anio, this.mes, this.dia);
	}

	public static Fecha hoy() {

		LocalDate fecha = LocalDate.now();

		return new Fecha(fecha.getDayOfMonth(), fecha.getMonthValue(), fecha.getYear());
	}

	/* Sobreescritura */
	/* StringBuilder */
	@Override
	public String toString() {

		StringBuilder s = new StringBuilder("Fecha: ");

		return s.append(this.dia + "/" + this.mes + "/" + this.anio).toString();
	}

	public boolean esMenor(Fecha fecha) {

		if (this.anio < fecha.anio) {

			return true;

		} else if (this.anio == fecha.anio && fecha.mes < fecha.mes) {

			return true;

		} else if (this.anio == fecha.anio && fecha.mes == fecha.mes && fecha.dia < fecha.dia) {

			return true;
		}
		return false;
	}

	public static long diasEntre(Fecha inicio, Fecha fin) {

		return DAYS.between(LocalDate.of(inicio.anio, inicio.mes, inicio.dia),
				LocalDate.of(fin.anio, fin.mes, fin.dia));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anio;
		result = prime * result + dia;
		result = prime * result + mes;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fecha other = (Fecha) obj;
		if (anio != other.anio)
			return false;
		if (dia != other.dia)
			return false;
		if (mes != other.mes)
			return false;
		return true;
	}
}