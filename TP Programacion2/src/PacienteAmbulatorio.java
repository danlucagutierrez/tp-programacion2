
import java.util.LinkedList;

public class PacienteAmbulatorio extends Paciente {

	private LinkedList<Tratamiento> tratamientos;

	public PacienteAmbulatorio(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento) {

		super.nombre = nombre;
		super.historiaClinicaPaciente = historiaClinicaPaciente;
		super.fechaNacimiento = fechaNacimiento;
		super.saldo = 0;
		this.tratamientos = new LinkedList<Tratamiento>();
	}

	@Override
	String retornarNombre() {

		return super.nombre;
	}

	@Override
	Fecha retornarFechaNacimiento() {

		return super.fechaNacimiento;
	}

	@Override
	int retornarHistoriaClinicaPaciente() {

		return super.historiaClinicaPaciente;
	}

	@Override
	double retornarSaldo() {

		return super.saldo;
	}

	@Override
	void modificarSaldo(double saldo) {

		super.saldo = this.saldo + saldo;
	}

	public void ponerEnCero() {

		super.saldo = 0;
	}

	void agregarTratamiento(int matricula, String nombreTratamiento) {

		this.tratamientos.addLast(new Tratamiento(matricula, nombreTratamiento));
	}

	@Override
	public String toString() {

		return super.nombre;
	}
}