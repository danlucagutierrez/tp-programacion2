
public class RegistroInternacion {

		private String area;
		private int numeroHabitacion;
		private Fecha fechaIngreso;
		private Fecha fechaAlta;
		private int importe;

		public RegistroInternacion(String area, Fecha fechaIngreso) {
			
			this.area = area;
			this.numeroHabitacion = 0;
			this.fechaIngreso = fechaIngreso;
			this.fechaAlta = null;
			this.importe = 0;	
		}
		
		public void modificarAltaInternacion(Fecha fechaAlta) {
			
			this.fechaAlta = fechaAlta;
		}
		
		public String retornarArea() {
			
			return this.area.toString();
		}

		public int retornarNumeroHabitacion() {
			
			return this.numeroHabitacion;
		} 

		public Fecha retornarFechaIngreso() {
			
			return this.fechaIngreso;
		}

		public Fecha retornarFechaAlta() {
			
			return this.fechaAlta;
		}

		public int retornarImporte() {
			
			return this.importe;
		}

		public void modificarImporte(int importe) {
			
			this.importe = importe;
		}
}