
import java.util.LinkedList;

public class PacientePrivado extends Paciente {

	private LinkedList<RegistroEspecialidad> registroEspecialidades;
	private LinkedList<Fecha> registrosGuardia;

	public PacientePrivado(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento) {

		super.nombre = nombre;
		super.historiaClinicaPaciente = historiaClinicaPaciente;
		super.fechaNacimiento = fechaNacimiento;
		super.saldo = 0;
		registroEspecialidades = new LinkedList<RegistroEspecialidad>();
		registrosGuardia = new LinkedList<Fecha>();

	}

	@Override
	String retornarNombre() {

		return this.nombre;
	}

	@Override
	Fecha retornarFechaNacimiento() {

		return this.fechaNacimiento;
	}

	public LinkedList<Fecha> retornarRegistroGuardia() {

		return this.registrosGuardia;
	}

	public LinkedList<RegistroEspecialidad> retornarRegistroEspecialidades() {

		return this.registroEspecialidades;
	}

	@Override
	int retornarHistoriaClinicaPaciente() {

		return this.historiaClinicaPaciente;
	}

	@Override
	double retornarSaldo() {

		return this.saldo;
	}

	@Override
	public void modificarSaldo(double saldo) {

		super.saldo = this.saldo + saldo;
	}

	@Override
	public void ponerEnCero() {

		super.saldo = 0;
	}

	// Agrega atencion de especialidad
	public void guardarRegistroEspecialidad(Fecha fecha, int matricula) {

		this.registroEspecialidades.addLast(new RegistroEspecialidad(fecha, matricula));
	}

	// Agrega atencion de guardia

	public void guardarRegistrosGuardia(Fecha fechaAtencionGuardia) {

		this.registrosGuardia.addLast(fechaAtencionGuardia);
	}

	@Override
	public String toString() {

		return super.nombre.toString();
	}
}