
public class RegistroEspecialidad {
	
	private int matricula;
	private Fecha fechaAtencion;

	public RegistroEspecialidad(Fecha fecha, int matricula) {
		
		this.matricula = matricula;
		this.fechaAtencion = fecha;
	}
	
	public int retornarMatricula() {
		
		return matricula;
	}

	public void modificarMatricula(int matricula) {
		
		this.matricula = matricula;
	}

	public Fecha retornarFechaAtencion() {
		
		return fechaAtencion;
	}

	public void modificarFechaAtencion(Fecha fechaAtencion) {
	
		this.fechaAtencion = fechaAtencion;
	}
}