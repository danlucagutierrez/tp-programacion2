
import java.util.LinkedList;

/*Herencia*/
public class PacienteObraSocial extends Paciente {

	private LinkedList<RegistroInternacion> listaInternacion;
	private String obraSocial;
	private double porcentaje;

	public PacienteObraSocial(String nombre, int historiaClinicaPaciente, Fecha fechaNacimiento, String obraSocial,
			double porcentaje) {

		this.listaInternacion = new LinkedList<RegistroInternacion>();
		super.nombre = nombre;
		super.fechaNacimiento = fechaNacimiento;
		super.historiaClinicaPaciente = historiaClinicaPaciente;
		super.saldo = 0;
		this.obraSocial = obraSocial;
		this.porcentaje = porcentaje;

	}

	public LinkedList<RegistroInternacion> retornarRegistroInternacion() {

		return this.listaInternacion;
	}

	/* Sobreescritura */
	@Override
	public String retornarNombre() {

		return super.nombre;
	}

	@Override
	public Fecha retornarFechaNacimiento() {

		return super.fechaNacimiento;
	}

	@Override
	public int retornarHistoriaClinicaPaciente() {

		return super.historiaClinicaPaciente;
	}

	@Override
	public double retornarSaldo() {

		return super.saldo;
	}

	public String retornarObraSocial() {

		return this.obraSocial;
	}

	public double retornarPorcentaje() {

		return this.porcentaje;
	}

	@Override
	public void modificarSaldo(double saldo) {

		super.saldo = this.saldo + saldo;
	}

	@Override
	public void ponerEnCero() {

		super.saldo = 0;
	}

	public void agregarIngresoInternacion(String area, Fecha fechaIngreso) {

		this.listaInternacion.addLast(new RegistroInternacion(area, fechaIngreso));
	}

	public void agregarAltaInternacion(Fecha fechaAlta) {

		if (this.listaInternacion.getLast().retornarFechaAlta() == null) {

			this.listaInternacion.getLast().modificarAltaInternacion(fechaAlta);
		}
	}

	@Override
	public String toString() {

		StringBuilder s = new StringBuilder(super.nombre);

		return s.append(" " + this.obraSocial).toString();
	}

	public boolean estaInternado() {
		
		if (!listaInternacion.isEmpty()) {
			
			if (listaInternacion.getLast().retornarFechaIngreso() != null && listaInternacion.getLast().retornarFechaAlta() == null) {
				
				return true;
				
			} else {
				
				return listaInternacion.getLast().retornarFechaAlta().esMenor(listaInternacion.getLast().retornarFechaIngreso());
			}
			
		}
		return false;
	}
}