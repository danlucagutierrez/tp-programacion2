
public class Medico {

	private String nombre;
	private int matricula;
	private String nombreEspecialidad;
	private double valorHonorario;

	/*Modificamos el constructor para que pida un String nombreEspecialidad*/
	public Medico(String nombre, int matricula, String nombreEspecialidad, double valorHonorario) {

		this.nombre = nombre;
		this.matricula = matricula;
		this.nombreEspecialidad = nombreEspecialidad;
		this.valorHonorario = valorHonorario;
	}

	public String retornarNombre() {

		return this.nombre;
	}

	public int retornarMatricula() {

		return this.matricula;
	}

	public String retornarEspecialidad() {

		return this.nombreEspecialidad;
	}

	public double retornarValorHonorario() {

		return this.valorHonorario;
	}

	@Override
	public String toString() {
		
		return this.nombre;
	}
}