
/*Clase abstracta*/
public abstract class Paciente {

	protected String nombre;
	protected Fecha fechaNacimiento;
	protected int historiaClinicaPaciente;
	protected double saldo;

	/* Metodos abstractos */
	abstract String retornarNombre();

	{
	}

	abstract Fecha retornarFechaNacimiento();

	{
	}

	abstract int retornarHistoriaClinicaPaciente();

	{
	}

	abstract double retornarSaldo();

	{
	}

	abstract void modificarSaldo(double saldo);

	{
	}

	public abstract String toString();

	{
	}

	abstract void ponerEnCero();

	{
	}
}