
public class Tratamiento {

	private int matricula;
	private String nombreTratamiento;

	public Tratamiento(int matricula, String nombreTratamiento) {

		this.matricula = matricula;
		this.nombreTratamiento = nombreTratamiento;
	}

	public String getNombreTratamiento() {
		
		return nombreTratamiento;
	}

	public int getMedico() {
		
		return matricula;
	}
}